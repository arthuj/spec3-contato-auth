package br.com.itau.usuario.service;

import br.com.itau.usuario.model.Contato;
import br.com.itau.usuario.repository.ContatoRepository;
import br.com.itau.usuario.security.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.stereotype.Service;

@Service
public class ContatoService {

    @Autowired
    ContatoRepository contatoRepository;

    @NewSpan("criar_contato")
    public Contato criarContato(@SpanTag(key = "contato") Contato contato){
        return contatoRepository.save(contato);
    }

    public Iterable<Contato> buscarContatos(Usuario usuario){
        return contatoRepository.findByUsuarioId(usuario.getId());
    }
}
