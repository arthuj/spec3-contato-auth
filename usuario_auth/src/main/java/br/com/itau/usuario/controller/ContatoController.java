package br.com.itau.usuario.controller;

import br.com.itau.usuario.model.Contato;
import br.com.itau.usuario.security.Usuario;
import br.com.itau.usuario.service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping
public class ContatoController {

    @Autowired
    private ContatoService contatoService;

    @PostMapping("/contato")
    public Contato criarContato(@RequestBody Contato contato, @AuthenticationPrincipal Usuario usuario){
        contato.setUsuarioNome(usuario.getNome());
        contato.setUsuarioId(usuario.getId());
        return contatoService.criarContato(contato);
    }
    @GetMapping("/contatos")
    public Iterable<Contato> buscarContatos(@AuthenticationPrincipal Usuario usuario){
        return contatoService.buscarContatos(usuario);
    }
}
