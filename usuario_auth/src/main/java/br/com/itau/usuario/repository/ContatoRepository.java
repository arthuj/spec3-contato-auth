package br.com.itau.usuario.repository;

import br.com.itau.usuario.model.Contato;
import br.com.itau.usuario.security.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface ContatoRepository extends CrudRepository<Contato,Long> {

    Iterable<Contato> findByUsuarioId(int id);
}
